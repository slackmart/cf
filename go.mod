module github.com/rendon/cf

go 1.13

require (
	github.com/codegangsta/cli v1.14.0
	github.com/fatih/color v1.5.1-0.20170926111411-5df930a27be2
	github.com/mattn/go-colorable v0.0.10-0.20171111065953-6fcc0c1fd9b6 // indirect
	github.com/mattn/go-isatty v0.0.4 // indirect
	github.com/rendon/asserting v0.0.0-20181021213634-d8a44593ca11
	github.com/rendon/testcli v0.0.0-20161027181003-6283090d169f
	github.com/sirupsen/logrus v1.4.2
	golang.org/x/net v0.0.0-20200301022130-244492dfa37a
	gopkg.in/yaml.v2 v2.2.8
)
